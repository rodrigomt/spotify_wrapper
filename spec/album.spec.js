import chai, { expect } from 'chai'
import SpotifyWrapper from '../src/index'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

chai.use(sinonChai)
global.fetch = require('node-fetch')

describe('Album', () => {

    let spotify
    let stubFetch
    let promise

    beforeEach(() => {
        spotify = new SpotifyWrapper({
            token: 'foo'
        })
        stubFetch = sinon.stub(global, 'fetch')
        promise = stubFetch.resolves({ json: () => ({ album: 'name' }) })
    })

    afterEach(() => {
        stubFetch.restore()
    })

    describe('smoke tests', () => {

        it('should have getAlbum method', () => {
            expect(spotify.album.getAlbum).to.exist
        })

        it('should have getAlbums method', () => {
            expect(spotify.album.getAlbums).to.exist
        })

        it('should have getTracks method', () => {
            expect(spotify.album.getTracks).to.exist
        })
    })

    describe('getAlbum', () => {
        it('should call fecth method', () => {
            const album = spotify.album.getAlbum()
            expect(stubFetch).to.have.been.calledOnce
        })

        it('should call fecth with the correct URL', () => {
            const album = spotify.album.getAlbum('4aawyAB9vmqN3uQ7FjRGTy')
            expect(stubFetch).to.have.been.calledWith('https://api.spotify.com/v1/albums/4aawyAB9vmqN3uQ7FjRGTy')

            const album2 = spotify.album.getAlbum('4aawyAB9vmqN3uQ7FjRGTk')
            expect(stubFetch).to.have.been.calledWith('https://api.spotify.com/v1/albums/4aawyAB9vmqN3uQ7FjRGTk')
        })

        it('should return the correct data from Promise', () => {
            const album = spotify.album.getAlbum('4aawyAB9vmqN3uQ7FjRGTy')
            album.then((data) => {
                expect(data).to.be.eql({ album: 'name' })
            })
        })
    })

    describe('getAlbums', () => {
        it('should call fetch method', () => {
            const albums = spotify.album.getAlbums()
            expect(stubFetch).to.have.been.calledOnce
        })

        it('should call fetch with the correct URL', () => {
            const albums = spotify.album.getAlbums(['4aawyAB9vmqN3uQ7FjRGTy', '4aawyAB9vmqN3uQ7FjRGTk'])
            expect(stubFetch).to.have.been.calledWith('https://api.spotify.com/v1/albums/?ids=4aawyAB9vmqN3uQ7FjRGTy,4aawyAB9vmqN3uQ7FjRGTk')
        })

        it('should return the correct data from promise', () => {
            const albums = spotify.album.getAlbums(['4aawyAB9vmqN3uQ7FjRGTy', '4aawyAB9vmqN3uQ7FjRGTk'])
            albums.then((data) => {
                expect(data).to.be.eql({ album: 'name' })
            })
        })
    })

    describe('getTracks', () => {
        it('should call fect method', () => {
            const tracks = spotify.album.getTracks()
            expect(stubFetch).to.have.been.calledOnce
        })

        it('should call fecth with the correct URL', () => {
            const tracks = spotify.album.getTracks('4aawyAB9vmqN3uQ7FjRGTy')
            expect(stubFetch).to.have.been.calledWith('https://api.spotify.com/v1/albums/4aawyAB9vmqN3uQ7FjRGTy/tracks')
        })

        it('should return the correct data from Promise', () => {
            const tracks = spotify.album.getTracks('4aawyAB9vmqN3uQ7FjRGTy')
            tracks.then((data) => {
              expect(data).to.be.eql({ album: 'name' })
            })
        })
    })

})