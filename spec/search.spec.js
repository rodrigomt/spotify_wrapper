import chai, { expect, should } from 'chai'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'

chai.use(sinonChai)
global.fetch = require('node-fetch')

import SpotifyWrapper from '../src/index'

describe('Search', () => {

  let spotify
  let fetchedStub
  let promise

  beforeEach(() => {
    spotify = new SpotifyWrapper({
      token: 'BQDoF6xM3oQyRveI_bfD7Ehe3NmZbFdHqNreaOBPp3PUGazO3mZgWTjvQZj7Aq1hToRjXkaDHZJApV8MRhOGEXsHYxXOxX4reGp8UUdvzHJ4jE5jBWC2WXNnXhAe4RSKPVmUE-JHIdChOaljQm5O-OxfZ-WTD6cBnE7OZUnC-z8gQ14EaytwMI6PZRG6HNc3jfe1KH44_FEuuzOQfYwtf-IcyD983Rtwe44G88f5Vj-cxnfKCt1uiQKAyIRCKq4gq7nQqtl4ciYGZfpxm0RNyxjktzYcFSft28J9'
    })
    fetchedStub = sinon.stub(global, 'fetch')
    promise = fetchedStub.resolves({ json: () => ({ artists: 'name'}) })
  })

  afterEach(() => {
    fetchedStub.restore()
  })

  describe('smoke tests', () => {

    it('should exist the spotify.search.albums method', () => {
      expect(spotify.search.albums).to.exist
    })

    it('should exist the spotify.search.artists method', () => {
      expect(spotify.search.artists).to.exist
    })

    it('should exist the spotify.search.tracks method', () => {
      expect(spotify.search.tracks).to.exist
    })

    it('should exist the spotify.search.playlist method', () => {
      expect(spotify.search.playlist).to.exist
    })

  })

  describe('spotify.search.artists', () => {

    it('should call fetch function', () => {
      const artists = spotify.search.artists('Incubus')
      expect(fetchedStub).to.have.been.calledOnce
    })

    // it('should call fetch with the correct URL', () => {
    //   const artists = spotify.search.artists('Ise+')
    //   expect(fetchedStub).to.have.been.calledWith('https://api.spotify.com/v1/search?q=Incubus&type=artists')

    //   const artists2 = spotify.search.artists('Muses')
    //   expect(fetchedStub).to.have.been.calledWith('https://api.spotify.com/v1/search?q=Muses&type=artists')
    // })
    
  })

  describe('spotify.search.albums', () => {
    it('should call fetch function', () => {
      const albums = spotify.search.albums('Incubus')
      expect(fetchedStub).to.have.been.calledOnce
    })

    it('should call fetch with the correct URL', () => {
      const albums = spotify.search.albums('Incubus')
      expect(fetchedStub).to.have.been.calledWith('https://api.spotify.com/v1/search?q=Incubus&type=album')

      const albums2 = spotify.search.albums('Muses')
      expect(fetchedStub).to.have.been.calledWith('https://api.spotify.com/v1/search?q=Incubus&type=album')
    })
  })

  describe('spotify.search.tracks', () => {
    it('should call fetch function', () => {
      const track = spotify.search.tracks('Incubus')
      expect(fetchedStub).to.have.been.calledOnce
    })

    it('should call fetch with the correct URL', () => {
      const track = spotify.search.tracks('Incubus')
      expect(fetchedStub).to.have.been.calledWith('https://api.spotify.com/v1/search?q=Incubus&type=track')

      const track2 = spotify.search.tracks('Muses')
      expect(fetchedStub).to.have.been.calledWith('https://api.spotify.com/v1/search?q=Incubus&type=track')
    })
  })

  describe('spotify.search.playlist', () => {
    it('should call fetch function', () => {
      const playlist = spotify.search.playlist('Incubus')
      expect(fetchedStub).to.have.been.calledOnce
    })

    it('should call fetch with the correct URL', () => {
      const playlist = spotify.search.playlist('Incubus')
      expect(fetchedStub).to.have.been.calledWith('https://api.spotify.com/v1/search?q=Incubus&type=playlist')

      const playlist2 = spotify.search.playlist('Muses')
      expect(fetchedStub).to.have.been.calledWith('https://api.spotify.com/v1/search?q=Incubus&type=playlist')
    })
  })
})
